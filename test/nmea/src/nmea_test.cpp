#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "nmea/floating_policy/floating.h"
#include "nmea/parse.h"

namespace {

using namespace ::testing;
using namespace nmea;

TEST(ParseTest, ParseGPGGA1) {
    auto result = parse<floating_policy::floating>(
        "$GPGGA,235317.000,4003.9039,N,10512.5793,W,1,08,1.6,1577.9,M,-20.7,M,,0000*5E\r\n");

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<gpgga_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->timestamp, Eq(85997));
    ASSERT_THAT(p->latitude, FloatEq(40.065065f));
    ASSERT_THAT(p->longitude, FloatEq(-105.209655f));
    ASSERT_THAT(p->quality, Eq(gps_quality::sps_mode));
    ASSERT_THAT(p->satellites, Eq(8));
    ASSERT_THAT(p->dilution, FloatEq(1.6f));
    ASSERT_THAT(p->altitude, FloatEq(1577.9f));
}

TEST(ParseTest, ParseGPGGA2) {
    auto result = parse<floating_policy::floating>(
        "$GPGGA,134658.00,5106.9792,N,11402.3003,W,2,09,1.0,1048.47,M,-16.27,M,08,AAAA*60\r\n");

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<gpgga_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->timestamp, Eq(49618));
    ASSERT_THAT(p->latitude, FloatEq(51.11632f));
    ASSERT_THAT(p->longitude, FloatEq(-114.0383333f));
    ASSERT_THAT(p->quality, Eq(gps_quality::differential_mode));
    ASSERT_THAT(p->satellites, Eq(9));
    ASSERT_THAT(p->dilution, FloatEq(1.0f));
    ASSERT_THAT(p->altitude, FloatEq(1048.47));
}

TEST(ParseTest, ParseGPGSA1) {
    auto result =
        parse<floating_policy::floating>("$GPGSA,A,3,01,03,04,17,,,,,,,,,3.54,2.93,1.97*07\r\n");

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<gpgsa_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->mode, Eq(nmea::gps_mode::automatic));
    ASSERT_THAT(p->fix, Eq(nmea::gps_fix_mode::fix_3d));
    ASSERT_THAT(p->satellites[0], Eq(1));
    ASSERT_THAT(p->satellites[1], Eq(3));
    ASSERT_THAT(p->satellites[2], Eq(4));
    ASSERT_THAT(p->satellites[3], Eq(17));
    ASSERT_THAT(p->position_dilution, FloatEq(3.54f));
    ASSERT_THAT(p->horizontal_dilution, FloatEq(2.93f));
    ASSERT_THAT(p->vertical_dilution, FloatEq(1.97f));
}

TEST(ParseTest, ParseGPGSA2) {
    auto result =
        parse<floating_policy::floating>("$GPGSA,A,1,01,03,04,,,,,,,,,,3.17,3.01,1.00*00\r\n");

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<gpgsa_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->mode, Eq(nmea::gps_mode::automatic));
    ASSERT_THAT(p->fix, Eq(nmea::gps_fix_mode::no_fix));
    ASSERT_THAT(p->satellites[0], Eq(1));
    ASSERT_THAT(p->satellites[1], Eq(3));
    ASSERT_THAT(p->satellites[2], Eq(4));
    ASSERT_THAT(p->position_dilution, FloatEq(3.17f));
    ASSERT_THAT(p->horizontal_dilution, FloatEq(3.01f));
    ASSERT_THAT(p->vertical_dilution, FloatEq(1.00f));
}

TEST(ParseTest, ParseInvalidStart) {
    auto result = parse<floating_policy::floating>(
        "GPGGA,235317.000,4003.9039,N,10512.5793,W,1,08,1.6,1577.9,M,-20.7,M,,0000*5E\r\n");

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::invalid_start));
}

TEST(ParseTest, ParseInvalidChecksum) {
    auto result = parse<floating_policy::floating>(
        "$GPGGA,235317.000,4003.9039,N,10512.5793,W,1,08,1.6,1577.9,M,-20.7,M,,0000*5F\r\n");

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::invalid_checksum));
}

TEST(ParseTest, ParseInvalidEnd) {
    auto result = parse<floating_policy::floating>(
        "$GPGGA,235317.000,4003.9039,N,10512.5793,W,1,08,1.6,1577.9,M,-20.7,M,,0000*5F");

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::invalid_end));
}

TEST(ParseTest, ParseInvalidAddress) {
    auto result = parse<floating_policy::floating>(
        "$XXXXX,235317.000,4003.9039,N,10512.5793,W,1,08,1.6,1577.9,M,-20.7,M,,0000*50\r\n");

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::invalid_address));
}

TEST(ParseTest, ParseGarbage) {
    auto result =
        parse<floating_policy::floating>("deadbeef$GPGGA,235317.000,4003.9039,N,10512.5793,W,1,08,"
                                         "1.6,1577.9,M,-20.7,M,,0000*5E\r\ndeadc0de");

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<gpgga_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->timestamp, Eq(85997));
    ASSERT_THAT(p->latitude, FloatEq(40.065065f));
    ASSERT_THAT(p->longitude, FloatEq(-105.209655f));
    ASSERT_THAT(p->quality, Eq(gps_quality::sps_mode));
    ASSERT_THAT(p->satellites, Eq(8));
    ASSERT_THAT(p->dilution, FloatEq(1.6f));
    ASSERT_THAT(p->altitude, FloatEq(1577.9f));
}

TEST(ParseTest, EmptyGPGGA) {
    auto result = parse<floating_policy::floating>("$GPGGA,,,,,,0,00,99.99,,,,,,*48\r\n");

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(error::parse_error));
}

} // namespace
