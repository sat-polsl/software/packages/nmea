#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "nmea/detail/parse_gpgsa.h"
#include "nmea/enums.h"
#include "nmea/floating_policy/floating.h"

namespace {

using namespace ::testing;
using namespace nmea::detail;
using namespace nmea::floating_policy;

TEST(ParseGPGSATest, Success) {
    auto result = parse_gpgsa<floating>("A,3,01,03,04,17,,,,,,,,,3.54,2.93,1.97*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<nmea::gpgsa_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->mode, Eq(nmea::gps_mode::automatic));
    ASSERT_THAT(p->fix, Eq(nmea::gps_fix_mode::fix_3d));
    ASSERT_THAT(p->satellites[0], Eq(1));
    ASSERT_THAT(p->satellites[1], Eq(3));
    ASSERT_THAT(p->satellites[2], Eq(4));
    ASSERT_THAT(p->satellites[3], Eq(17));
    ASSERT_THAT(p->position_dilution, FloatEq(3.54f));
    ASSERT_THAT(p->horizontal_dilution, FloatEq(2.93f));
    ASSERT_THAT(p->vertical_dilution, FloatEq(1.97f));
}

TEST(ParseGPGSATest, ModeParseFailure) {
    auto result = parse_gpgsa<floating>("c,3,01,03,04,17,,,,,,,,,3.54,2.93,1.97*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGSATest, FixParseFailure) {
    auto result = parse_gpgsa<floating>("A,deadbeef,01,03,04,17,,,,,,,,,3.54,2.93,1.97*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGSATest, SatelliteParseFailure) {
    auto result = parse_gpgsa<floating>("A,3,01,deadcode,04,17,,,,,,,,,3.54,2.93,1.97*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGSATest, PositionalDilutionParseFailure) {
    auto result = parse_gpgsa<floating>("c,3,01,03,04,17,,,,,,,,,pdop,2.93,1.97*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGSATest, HorizontalDilutionParseFailure) {
    auto result = parse_gpgsa<floating>("c,3,01,03,04,17,,,,,,,,,3.54,hdop,1.97*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGSATest, VerticalDilutionParseFailure) {
    auto result = parse_gpgsa<floating>("c,3,01,03,04,17,,,,,,,,,3.54,2.93,vdop*07\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

} // namespace
