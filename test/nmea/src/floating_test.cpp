#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "nmea/floating_policy/floating.h"

namespace {

using namespace ::testing;
using namespace nmea::floating_policy;

TEST(FloatingPolicyTest, ParsePositive) {
    auto result = floating::parse("123.456");
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(123.456f));
}

TEST(FloatingPolicyTest, ParseNegative) {
    auto result = floating::parse("-123.456");
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(-123.456f));
}

TEST(FloatingPolicyTest, ParseZero) {
    auto result = floating::parse("0.0");
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(0.0f));
}

TEST(FloatingPolicyTest, ParseInt) {
    auto result = floating::parse("123");
    ASSERT_THAT(result.has_value(), Eq(true));
    ASSERT_THAT(result.value(), Eq(123.0f));
}

TEST(FloatingPolicyTest, ParseError) {
    auto result = floating::parse("abcde");
    ASSERT_THAT(result.has_value(), Eq(false));
}

} // namespace
