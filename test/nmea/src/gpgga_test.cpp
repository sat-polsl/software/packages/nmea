#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "nmea/detail/parse_gpgga.h"
#include "nmea/enums.h"
#include "nmea/floating_policy/floating.h"

namespace {

using namespace ::testing;
using namespace nmea::detail;
using namespace nmea::floating_policy;

TEST(ParseGPGGATest, Success) {
    auto result = parse_gpgga<floating>(
        "161229.000,3723.2475,S,12158.3416,E,1,07,1.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(true));
    auto* p = std::get_if<nmea::gpgga_message<float>>(&result.value());
    ASSERT_THAT(p, Ne(nullptr));
    ASSERT_THAT(p->timestamp, Eq(58349));
    ASSERT_THAT(p->latitude, FloatEq(-37.3874583f));
    ASSERT_THAT(p->longitude, FloatEq(121.97236f));
    ASSERT_THAT(p->quality, Eq(nmea::gps_quality::sps_mode));
    ASSERT_THAT(p->satellites, Eq(7));
    ASSERT_THAT(p->dilution, FloatEq(1.5f));
    ASSERT_THAT(p->altitude, FloatEq(1234.1));
}

TEST(ParseGPGGATest, UTCParseFailure) {
    auto result = parse_gpgga<floating>(
        "1dead.000,3723.2475,S,12158.3416,E,1,07,1.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, LatitudeParseFailure) {
    auto result = parse_gpgga<floating>(
        "129.000,37beef23.2475,S,12158.3416,E,1,07,1.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, LongitudeParseFailure) {
    auto result = parse_gpgga<floating>(
        "129.000,3723.2475,S,12158.34c0de16,E,1,07,1.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, FixQuailityParseFailure) {
    auto result = parse_gpgga<floating>(
        "129.000,3723.2475,S,12158.3416,E,1beef,07,1.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, SatellitesParseFailure) {
    auto result = parse_gpgga<floating>(
        "129.000,3723.2475,S,12158.3416,E,1,0dead7,1.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, DilutionParseFailure) {
    auto result = parse_gpgga<floating>(
        "129.000,3723.2475,S,12158.3416,E,1,08,1feed.5,1234.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, AltitudeParseFailure) {
    auto result = parse_gpgga<floating>(
        "129.000,3723.2475,S,12158.3416,E,1,08,1.5,12beef34.1,M,,,,0000*18\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

TEST(ParseGPGGATest, EmptyMessage) {
    auto result = parse_gpgga<floating>(",,,,,0,00,99.99,,,,,,*48\r\n", {});

    ASSERT_THAT(result.has_value(), Eq(false));
    ASSERT_THAT(result.error(), Eq(nmea::error::parse_error));
}

} // namespace
