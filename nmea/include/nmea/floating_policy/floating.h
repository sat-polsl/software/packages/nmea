#pragma once
#include <cmath>
#include <cstdint>
#include <string_view>
#include "nmea/detail/characters.h"
#include "nmea/enums.h"
#include "nmea/floating_policy/policy_concept.h"
#include "satext/charconv.h"
#include "satext/expected.h"

namespace nmea::floating_policy {

class floating {
public:
    using type = float;

    static constexpr satext::expected<type, error> parse(std::string_view input) {
        auto delimiter_pos = input.find_first_of(detail::character::float_delimiter);
        auto integral_view = input.substr(0, delimiter_pos);
        auto fractional_view = get_fractional_part(input, delimiter_pos);

        std::int32_t integral{};
        if (auto [end, err] = satext::from_chars(integral_view, integral);
            err != std::errc{} || end != integral_view.end()) {
            return satext::unexpected{error::parse_error};
        }

        std::int32_t fractional{};
        if (!fractional_view.empty()) {
            if (auto [end, err] = satext::from_chars(fractional_view, fractional);
                err != std::errc{} || end != fractional_view.end()) {
                return satext::unexpected{error::parse_error};
            }
        }

        auto result = static_cast<float>(integral);
        float fractional_result =
            static_cast<float>(fractional) / maximum_fractional_value(fractional_view.size());

        return result + (result >= 0 ? fractional_result : -fractional_result);
    }

    static constexpr type inverse(type value) { return -1.0f * value; }

    static constexpr type ddmm_to_degrees(type value) {
        value *= ddmm_to_dd_dot_mm;

        auto integral = static_cast<float>(static_cast<int32_t>(value));
        auto fractional = std::fabs(value - integral) * minutes_to_degrees;

        return integral + (integral >= 0.0f ? fractional : -fractional);
    }

private:
    static constexpr float minutes_to_degrees = 100.0f / 60.0f;
    static constexpr float ddmm_to_dd_dot_mm = 0.01f;

    static constexpr float maximum_fractional_value(std::size_t number_of_digits) {
        return std::pow(10.0f, static_cast<float>(number_of_digits));
    }

    static constexpr std::string_view get_fractional_part(std::string_view input,
                                                          std::size_t delimieter_pos) {
        if (delimieter_pos != std::string_view::npos) {
            auto fractional_pos = delimieter_pos + 1;
            return input.substr(fractional_pos, input.size() - fractional_pos);
        } else {
            return {};
        }
    }
};

static_assert(floating_policy_concept<floating>);

} // namespace nmea::floating_policy
