#pragma once
#include <concepts>
#include "nmea/enums.h"
#include "satext/expected.h"

namespace nmea::floating_policy {

// clang-format off
template<typename T>
concept floating_policy_concept = requires(std::string_view input, typename T::type value) {
    { T::parse(input) } -> std::same_as<satext::expected<typename T::type, nmea::error>>;
    { T::inverse(value) } -> std::same_as<typename T::type>;
    { T::ddmm_to_degrees(value) } -> std::same_as<typename T::type>;
};
// clang-format on

} // namespace nmea::floating_policy
