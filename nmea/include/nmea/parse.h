#pragma once
#include <string_view>
#include "nmea/detail/characters.h"
#include "nmea/detail/checksum.h"
#include "nmea/detail/parse_address.h"
#include "nmea/detail/parse_gpgga.h"
#include "nmea/detail/parse_gpgsa.h"
#include "nmea/floating_policy/policy_concept.h"
#include "nmea/types.h"
#include "satext/overload.h"

namespace nmea {
/**
 * @ingroup nmea
 * @{
 */

/**
 * @brief Parses given input to NMEA messages.
 * @tparam FloatPolicy Floating/Fixed point policy.
 * @param input Buffer.
 * @return Variant containing parsed message on success, error otherwise.
 */
template<floating_policy::floating_policy_concept FloatPolicy>
parse_result<typename FloatPolicy::type> parse(std::string_view input) {
    using floating = typename FloatPolicy::type;

    if (input.empty()) {
        return satext::unexpected{error::empty};
    }

    auto start = input.find_first_of(detail::character::start);

    if (start == std::string_view::npos) {
        return satext::unexpected{error::invalid_start};
    }

    auto end = input.find_first_of(detail::character::terminate);

    if (end == std::string_view::npos) {
        return satext::unexpected{error::invalid_end};
    }

    auto sentence_view = input.substr(start, end - start);
    if (!detail::verify_checksum(sentence_view)) {
        return satext::unexpected{error::invalid_checksum};
    }
    return detail::parse_address<FloatPolicy>(sentence_view)
        .and_then([](auto value) -> parse_result<floating> {
            auto [result, data] = value;
            return result.and_then([data](auto message_variant) -> parse_result<floating> {
                return std::visit(satext::overload(
                                      [data](gpgga_message<floating> msg) {
                                          return detail::parse_gpgga<FloatPolicy>(data, msg);
                                      },
                                      [data](gpgsa_message<floating> msg) {
                                          return detail::parse_gpgsa<FloatPolicy>(data, msg);
                                      }),
                                  message_variant);
            });
        });
}

/** @} */
} // namespace nmea
