#pragma once

namespace nmea {

/**
 * @ingroup nmea
 * @{
 */

/**
 * @brief NMEA parsing error enumeration.
 */
enum class error {
    empty,
    invalid_start,
    invalid_address,
    invalid_end,
    parse_error,
    invalid_checksum
};

/**
 * @brief GPS Quality enumeration.
 */
enum class gps_quality {
    not_available,
    sps_mode,
    differential_mode,
    pps_mode,
    rtk,
    float_rtk,
    estimated_mode,
    manual_input,
    simulator_mode
};

/**
 * @brief GPS Mode.
 */
struct gps_mode {
    static constexpr char manual = 'M';
    static constexpr char automatic = 'A';
};

/**
 * @brief GPS Mode enumeration.
 */
enum class gps_fix_mode { no_fix = 1, fix_2d = 2, fix_3d = 3 };

/** @} */

} // namespace nmea
