#pragma once
#include <array>
#include <cstdint>
#include <variant>
#include "nmea/enums.h"
#include "nmea/floating_policy/policy_concept.h"
#include "satext/expected.h"

namespace nmea {

/**
 * @ingroup nmea
 * @{
 */

/**
 * @brief GPGGA Message.
 *
 * @remark NMEA 0183 Standard For Interfacing Marine Electronic Devices V3.01, 1.1.2002, Page 42
 *
 * @tparam Float Float type.
 */
template<typename Float>
struct gpgga_message {
    /**
     * @brief UTC timestamp in seconds from 0:00.
     */
    std::uint32_t timestamp;

    /**
     * @brief Latitude in degrees, positive for northern hemisphere, negative for southern
     * hemisphere.
     */
    Float latitude;

    /**
     * @brief Longitude in degrees, positive for eastern hemisphere, negative for western
     * hemisphere.
     */
    Float longitude;

    /**
     * @brief GPS Fix quality.
     */
    gps_quality quality;

    /**
     * @brief Visible satellites.
     */
    std::uint32_t satellites;

    /**
     * @brief Measurement dilution in meters.
     */
    Float dilution;

    /**
     * @brief Altitude in meters.
     */
    Float altitude;
};

template<typename Float>
struct gpgsa_message {
    /**
     * @brief GPS operating mode.
     */
    char mode;

    /**
     * @brief Fix mode.
     */
    gps_fix_mode fix;

    /**
     * @brief Satellite IDs.
     */
    std::array<std::uint32_t, 12> satellites;

    /**
     * @brief Position dilution.
     */
    Float position_dilution;

    /**
     * @brief Horizontal dilution.
     */
    Float horizontal_dilution;

    /**
     * @brief Vertical dilution.
     */
    Float vertical_dilution;
};

/**
 * @brief Messages helper struct.
 * @tparam FloatingPolicy Floating policy.
 */
template<floating_policy::floating_policy_concept FloatingPolicy>
struct messages {
    using floating_type = typename FloatingPolicy::type;
    using gpgga = gpgga_message<floating_type>;
    using gpgsa = gpgsa_message<floating_type>;
};

/**
 * @brief Message variant.
 */
template<typename Float>
using message = std::variant<gpgga_message<Float>, gpgsa_message<Float>>;

/**
 * @brief Parse result.
 */
template<typename Float>
using parse_result = satext::expected<message<Float>, error>;

/** @} */

} // namespace nmea
