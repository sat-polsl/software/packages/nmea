#pragma once
#include <string_view>
#include <tuple>
#include "nmea/detail/characters.h"
#include "nmea/detail/nmea_defines.h"
#include "nmea/floating_policy/policy_concept.h"
#include "nmea/types.h"
#include "satext/constexpr_map.h"

namespace nmea::detail {

template<floating_policy::floating_policy_concept FloatPolicy>
using parse_address_result = std::tuple<parse_result<typename FloatPolicy::type>, std::string_view>;

template<floating_policy::floating_policy_concept FloatPolicy>
satext::expected<parse_address_result<FloatPolicy>, error> parse_address(std::string_view input) {
    using floating = typename FloatPolicy::type;

    static constexpr satext::constexpr_map<std::string_view, message<floating>, 2> message_map = {
        {"GPGGA", gpgga_message<floating>{}}, {"GPGSA", gpgsa_message<floating>{}}};

    if (input[start_pos] != character::start) {
        return satext::unexpected{error::invalid_start};
    }

    auto delimiter = input.find_first_of(character::delimiter);
    auto address = input.substr(address_pos, delimiter - address_pos);

    if (message_map.contains(address)) {
        return parse_address_result<FloatPolicy>{message_map[address], input.substr(delimiter + 1)};
    } else {
        return satext::unexpected{error::invalid_address};
    }
}

} // namespace nmea::detail
