#pragma once
#include <string_view>

namespace nmea::detail {

struct character {
    static constexpr char delimiter = ',';
    static constexpr char float_delimiter = '.';
    static constexpr char start = '$';
    static constexpr char checksum = '*';
    static constexpr char terminate = '\n';
};

} // namespace nmea::detail
