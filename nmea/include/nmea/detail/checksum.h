#pragma once
#include <string_view>
#include "nmea/detail/characters.h"
#include "nmea/detail/nmea_defines.h"
#include "satext/charconv.h"

namespace nmea::detail {

constexpr bool verify_checksum(std::string_view input) {
    if (input[start_pos] != character::start) {
        return false;
    }

    auto checksum_pos = input.find_first_of(character::checksum);
    if (checksum_pos == std::string_view::npos) {
        return false;
    }

    std::byte xor_value{};
    auto payload = input.substr(address_pos, checksum_pos - address_pos);
    for (auto payload_span = std::span(payload); auto&& b : std::as_bytes(payload_span)) {
        xor_value ^= b;
    }

    auto checksum_view = input.substr(checksum_pos + 1, checksum_size);

    std::uint8_t checksum{};
    if (auto [_, err] = satext::from_chars(checksum_view, checksum, 16); err != std::errc{}) {
        return false;
    }

    return xor_value == std::byte{checksum};
}

} // namespace nmea::detail
