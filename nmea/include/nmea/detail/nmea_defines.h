#pragma once

namespace nmea::detail {

constexpr std::size_t start_pos = 0;
constexpr std::size_t address_pos = 1;
constexpr std::size_t checksum_size = 2;

} // namespace nmea::detail
