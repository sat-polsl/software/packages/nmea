#pragma once
#include <string_view>
#include "nmea/detail/utility.h"
#include "nmea/enums.h"
#include "nmea/floating_policy/policy_concept.h"
#include "nmea/types.h"
#include "satext/charconv.h"

namespace nmea::detail {

namespace gpgsa {

template<floating_policy::floating_policy_concept FloatPolicy>
using gpgsa_parse_step_type =
    std::tuple<gpgsa_message<typename FloatPolicy::type>, std::string_view>;

template<floating_policy::floating_policy_concept FloatPolicy>
using gpgsa_parse_step_result = satext::expected<gpgsa_parse_step_type<FloatPolicy>, error>;

template<floating_policy::floating_policy_concept FloatPolicy>
gpgsa_parse_step_result<FloatPolicy> parse_mode(gpgsa_parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [field, tail] = next_field(input);

    if (field.size() != 1) {
        return satext::unexpected{error::parse_error};
    }

    auto mode = field[0];
    if (mode != gps_mode::automatic && mode != gps_mode::manual) {
        return satext::unexpected{error::parse_error};
    }

    result.mode = mode;

    return gpgsa_parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
gpgsa_parse_step_result<FloatPolicy> parse_fix(gpgsa_parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [field, tail] = next_field(input);

    std::uint32_t parsed{};
    if (auto [end, err] = satext::from_chars(field, parsed);
        err != std::errc{} || end != field.end()) {
        return satext::unexpected{error::parse_error};
    }

    result.fix = gps_fix_mode(parsed);

    return gpgsa_parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
gpgsa_parse_step_result<FloatPolicy> parse_satellites(gpgsa_parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;

    for (auto& sat : result.satellites) {
        auto [field, tail] = next_field(input);
        input = tail;

        if (field.empty()) {
            continue;
        }

        std::uint32_t parsed{};
        if (auto [end, err] = satext::from_chars(field, parsed);
            err != std::errc{} || end != field.end()) {
            return satext::unexpected{error::parse_error};
        }

        sat = parsed;
    }

    return gpgsa_parse_step_type<FloatPolicy>{result, input};
    return value;
}

template<floating_policy::floating_policy_concept FloatPolicy>
gpgsa_parse_step_result<FloatPolicy>
parse_position_dilution(gpgsa_parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [dilution, tail] = next_field(input);

    auto dilution_result = FloatPolicy::parse(dilution);
    if (!dilution_result.has_value()) {
        return satext::unexpected{dilution_result.error()};
    }
    result.position_dilution = dilution_result.value();

    return gpgsa_parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
gpgsa_parse_step_result<FloatPolicy>
parse_horizontal_dilution(gpgsa_parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [dilution, tail] = next_field(input);

    auto dilution_result = FloatPolicy::parse(dilution);
    if (!dilution_result.has_value()) {
        return satext::unexpected{dilution_result.error()};
    }
    result.horizontal_dilution = dilution_result.value();

    return gpgsa_parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
gpgsa_parse_step_result<FloatPolicy>
parse_vertical_dilution(gpgsa_parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [dilution, tail] = next_field(input);

    auto dilution_result = FloatPolicy::parse(dilution);
    if (!dilution_result.has_value()) {
        return satext::unexpected{dilution_result.error()};
    }
    result.vertical_dilution = dilution_result.value();

    return gpgsa_parse_step_type<FloatPolicy>{result, tail};
}
} // namespace gpgsa

template<floating_policy::floating_policy_concept FloatPolicy>
parse_result<typename FloatPolicy::type>
parse_gpgsa(std::string_view input, gpgsa_message<typename FloatPolicy::type> msg) {
    using floating = typename FloatPolicy::type;

    return gpgsa::parse_mode<FloatPolicy>({msg, input})
        .and_then(gpgsa::parse_fix<FloatPolicy>)
        .and_then(gpgsa::parse_satellites<FloatPolicy>)
        .and_then(gpgsa::parse_position_dilution<FloatPolicy>)
        .and_then(gpgsa::parse_horizontal_dilution<FloatPolicy>)
        .and_then(gpgsa::parse_vertical_dilution<FloatPolicy>)
        .map([](auto value) -> message<floating> { return std::get<0>(value); });
}

} // namespace nmea::detail
