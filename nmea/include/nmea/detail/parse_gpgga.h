#pragma once
#include <string_view>
#include "nmea/detail/utility.h"
#include "nmea/floating_policy/policy_concept.h"
#include "nmea/types.h"
#include "satext/charconv.h"

namespace nmea::detail {

namespace gpgga {
template<floating_policy::floating_policy_concept FloatPolicy>
using parse_step_type = std::tuple<gpgga_message<typename FloatPolicy::type>, std::string_view>;

template<floating_policy::floating_policy_concept FloatPolicy>
using parse_step_result = satext::expected<parse_step_type<FloatPolicy>, error>;

template<floating_policy::floating_policy_concept FloatPolicy>
satext::expected<std::uint32_t, error> parse_utc_field(std::string_view field) {
    static constexpr std::uint32_t seconds_mask = 100;
    static constexpr std::uint32_t minutes_mask = 10000;
    static constexpr std::uint32_t hours_mask = 1000000;
    static constexpr std::uint32_t seconds_in_hour = 3600;
    static constexpr std::uint32_t seconds_in_minute = 60;

    auto utc_result = FloatPolicy::parse(field);
    if (!utc_result.has_value()) {
        return satext::unexpected{utc_result.error()};
    }

    auto parsed = static_cast<std::uint32_t>(utc_result.value());

    auto seconds = parsed % seconds_mask;
    auto minutes = ((parsed % minutes_mask) - seconds) / 100;
    auto hours = ((parsed % hours_mask) - minutes - seconds) / 10000;

    return (hours * seconds_in_hour) + (minutes * seconds_in_minute) + seconds;
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_utc(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [field, tail] = next_field(input);

    auto utc_result = parse_utc_field<FloatPolicy>(field);
    if (!utc_result.has_value()) {
        return satext::unexpected{utc_result.error()};
    }
    result.timestamp = utc_result.value();

    return parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_latitude(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [latitude, tail_with_direction] = next_field(input);
    auto [direction, tail] = next_field(tail_with_direction);

    auto latitude_result = FloatPolicy::parse(latitude).map(FloatPolicy::ddmm_to_degrees);
    if (!latitude_result.has_value()) {
        return satext::unexpected{latitude_result.error()};
    }
    result.latitude = latitude_result.value();
    if (direction == "S") {
        result.latitude = FloatPolicy::inverse(result.latitude);
    }

    return parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_longitude(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [longitude, tail_with_direction] = next_field(input);
    auto [direction, tail] = next_field(tail_with_direction);

    auto longitude_result = FloatPolicy::parse(longitude).map(FloatPolicy::ddmm_to_degrees);
    if (!longitude_result.has_value()) {
        return satext::unexpected{longitude_result.error()};
    }
    result.longitude = longitude_result.value();
    if (direction == "W") {
        result.longitude = FloatPolicy::inverse(result.longitude);
    }

    return parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_gps_quality(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [field, tail] = next_field(input);

    std::uint32_t parsed{};
    if (auto [end, err] = satext::from_chars(field, parsed);
        err != std::errc{} || end != field.end()) {
        return satext::unexpected{error::parse_error};
    }

    result.quality = static_cast<gps_quality>(parsed);

    return parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_satellites(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [field, tail] = next_field(input);

    std::uint32_t parsed{};
    if (auto [end, err] = satext::from_chars(field, parsed);
        err != std::errc{} || end != field.end()) {
        return satext::unexpected{error::parse_error};
    }

    result.satellites = parsed;

    return parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_dilution(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [dilution, tail] = next_field(input);

    auto dilution_result = FloatPolicy::parse(dilution);
    if (!dilution_result.has_value()) {
        return satext::unexpected{dilution_result.error()};
    }
    result.dilution = dilution_result.value();

    return parse_step_type<FloatPolicy>{result, tail};
}

template<floating_policy::floating_policy_concept FloatPolicy>
parse_step_result<FloatPolicy> parse_altitude(parse_step_type<FloatPolicy> value) {
    auto [result, input] = value;
    auto [altitude, tail_with_unit] = next_field(input);
    auto [_, tail] = next_field(tail_with_unit);

    auto altitude_result = FloatPolicy::parse(altitude);
    if (!altitude_result.has_value()) {
        return satext::unexpected{altitude_result.error()};
    }
    result.altitude = altitude_result.value();

    return parse_step_type<FloatPolicy>{result, tail};
}

} // namespace gpgga

template<floating_policy::floating_policy_concept FloatPolicy>
parse_result<typename FloatPolicy::type>
parse_gpgga(std::string_view input, gpgga_message<typename FloatPolicy::type> msg) {
    using floating = typename FloatPolicy::type;

    return gpgga::parse_utc<FloatPolicy>({msg, input})
        .and_then(gpgga::parse_latitude<FloatPolicy>)
        .and_then(gpgga::parse_longitude<FloatPolicy>)
        .and_then(gpgga::parse_gps_quality<FloatPolicy>)
        .and_then(gpgga::parse_satellites<FloatPolicy>)
        .and_then(gpgga::parse_dilution<FloatPolicy>)
        .and_then(gpgga::parse_altitude<FloatPolicy>)
        .map([](auto value) -> message<floating> { return std::get<0>(value); });
}

} // namespace nmea::detail
