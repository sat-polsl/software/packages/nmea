#pragma once
#include <array>
#include <string_view>
#include <tuple>
#include "nmea/detail/characters.h"

namespace nmea::detail {

constexpr std::tuple<std::string_view, std::string_view> next_field(std::string_view input) {
    constexpr std::array<char, 3> delimiters = {character::delimiter, character::checksum, 0};

    auto delimiter = input.find_first_of(delimiters.data());

    if (delimiter == std::string_view::npos) {
        return {std::string_view{}, input};
    }

    auto field = input.substr(0, delimiter);

    return {field, input.substr(delimiter + 1)};
}

} // namespace nmea::detail
