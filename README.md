[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_nmea&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=sat-polsl_nmea)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=sat-polsl_nmea&metric=coverage)](https://sonarcloud.io/summary/new_code?id=sat-polsl_nmea)
[![Pipeline](https://gitlab.com/sat-polsl/software/packages/nmea/badges/main/pipeline.svg)](https://gitlab.com/sat-polsl/software/packages/nmea/-/pipelines?page=1&scope=all&ref=main)
[![Version](https://img.shields.io/badge/dynamic/json?color=blue&label=Version&query=%24%5B%3A1%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F43382552%2Frepository%2Ftags)](https://gitlab.com/sat-polsl/software/packages/nmea/-/tags)

# NMEA

## Overview

This package provides nmea parsing functionality.

Based on [NMEA 0183](http://www.plaisance-pratique.com/IMG/pdf/NMEA0183-2.pdf).

### Rules

1. Use conventional commits.
2. Update tag with version after merging to `main`.
3. Use `snake_case` for everything except template arguments which are named with `CamelCase`

## Usage:

```c++
std::array<128, char> buffer; // buffer with data
std::string_view buffer_view(buffer.data(), buffer.size());

using policy = nmea::floating_policy::floating;
using messages = nmea::messages<policy>;

nmea::parse<policy>(buffer_view)
    .map([](auto message) {
        std::visit(satext::overload([](messages::gpgga gpgga_message) {
                       // do stuff with gpgga message
                   }),
                   message);
    })
    .map_error([](nmea::error error) {
        // do stuff on error
    });
```

